import React from 'react'
import ItemMovie from './ItemMovie'
import {List} from 'antd'

function ListMovie(props){
    return(
        <div style={{minHight: '300 px'}}>
        <List
        pagination={{
            pageSize: 40,
          }}
        grid={{gutter: 16, column: 4}}
        dataSource={props.items}
        renderItem={item => (
            <List.Item>
                <ItemMovie item={item} />
            </List.Item>
        )}
        />
        </div>
    )
}
export default ListMovie