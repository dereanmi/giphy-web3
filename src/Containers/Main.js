import React, { Component } from 'react';
import { Spin, Modal, Button, Layout, Menu, message, Col, Row } from 'antd';
//import ListMovie from '../Components/ListMovie'
import RouteMenu from './RouteMenu';
import { connect } from 'react-redux';

import { Input } from 'antd';
const Search = Input.Search;

var GphApiClient = require('giphy-js-sdk-core')
const client = GphApiClient("kQMTY45oyABcpk6SBarCPcMk2Im9gBPM&limit=400&rating=G")

const { Header, Content, Footer } = Layout;
const menus = ['movies', 'favorite', 'profile'];


const mapStateToProps = state => {
    console.log(state.isShowDialog)
    return {
        isShowDialog: state.isShowDialog,
        itemMovieDetail: state.itemMovieDetail
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onDismissDialog: () => dispatch({ type: 'dismiss_dialog' }),
        onItemMovieClick: item =>
            dispatch({
                type: 'click_item',
                payload: item
            })
    };
};

class Main extends Component {
    state = {
        items: [],
        isShowModal: false,
        itemMovie: null,
        pathName: menus[0],
        favItems: []
    };
    onItemMovieClick = (item) => {
        this.setState({ isShowModal: true, itemMovie: item }, (a) => {
            console.log(a)
        })
    }
    onModelClickok = () => {
        //handle something click ok
        this.props.onDismissDialog();
    }
    onClickCancel = () => {
        this.props.onDismissDialog();
    }
    componentDidMount() {
        const jsonStr = localStorage.getItem('list-fav')
        if (jsonStr) {
            const items = JSON.parse(jsonStr)
            this.setState({ favItems: items })

        }
        const { pathname } = this.props.location;
        var pathName = menus[0];
        if (pathname != '/') {
            pathName = pathname.replace('/', '');
            if (!menus.includes(pathName)) pathName = menus[0];
        }
        this.setState({ pathName });
        fetch('https://api.giphy.com/v1/gifs/search?api_key=kQMTY45oyABcpk6SBarCPcMk2Im9gBPM&q=animal&limit=100&offset=0&rating=G&lang=en')
            .then(response => response.json())
            .then(movies => this.setState({ items: movies.data }));
    }

    onMenuClick = e => {
        var path = '/';
        if (e.key != 'home') {
            path = `/${e.key}`;
        }
        this.props.history.replace(path);
        //save path to state
    };

    onClickFavorite = () => {
        const itemClick = this.props.itemMovieDetail
        const items = this.state.favItems

        const result = items.find(item => {
            return item.title === itemClick.title
        })
        if (result) {
            message.error('This item added favorite')
        } else {
            items.push(itemClick)
            //console.log(items)
            localStorage.setItem('list-fav', JSON.stringify(items))
            message.success('Save your favorite Gif', 1);
            this.onClickCancel()
        }//เช็คไม่ให้add favชื่อซ้ำ  ถ้าไม่มีการaddจะโชวว่าThis item added favorite
    }

    onClickCopy = () => {
        const item = this.props.itemGiphyClick;
        navigator.clipboard.writeText(item.images.fixed_width.url)
        message.success('Copy your giphy', 1);
    }

    searchGiphy = (value) => {
        console.log("value", value)
        client.search('gifs', { "q": value })
            .then((response) => {
                response.data.forEach((gifObject) => {
                    console.log(gifObject)
                })
                this.setState({ items: response.data })
            })
            .catch((err) => {

            })
    }


    render() {
        const item = this.props.itemMovieDetail;/////
        console.log(item)
        //console.log('items:', this.state.items)
        return (

            <div style={{ width: '100%' }}>
                {this.state.items.length > 0 ? (
                    <div style={{ height: '100vh' }}>
                        {' '}
                        <Layout className="layout" style={{ background: 'white' }}>



                            <Header

                                style={{
                                    padding: '0px',
                                    position: 'fixed',
                                    zIndex: 1,
                                    width: '100%',
                                    left: '0px'
                                }}
                            >
                                
                                    <Col span={10} >
                                        <Menu
                                            theme="dark"
                                            mode="horizontal"
                                            defaultSelectedKeys={[this.state.pathName]}
                                            style={{ lineHeight: '64px' , marginRight: "200px"}}
                                            onClick={e => {
                                                this.onMenuClick(e);
                                            }}
                                        >
                                            <Menu.Item key={menus[0]}>Home</Menu.Item>
                                            <Menu.Item key={menus[1]}>Favorite</Menu.Item>
                                            <Menu.Item key={menus[2]}>Profile</Menu.Item>

                                        </Menu>
                                    </Col>
                                    <Row type="flex" justify="space-around" align="middle">
                                    <Col span={12}>
                                        <Search
                                            placeholder="input search text"
                                            onSearch={this.searchGiphy}
                                            enterButton
                                            style={{ marginTop: "15px", marginLeft: "200px"}}
                                        />
                                    </Col>
                                </Row>
                                <p></p>
                                <div > </div>
                            </Header>

                            <Content
                                style={{
                                    padding: '16px',
                                    marginTop: 64,
                                    minHeight: '600px',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    display: 'flex'
                                }}
                            >
                                <RouteMenu
                                    items={this.state.items}
                                //onItemMovieClick={this.onItemMovieClick}
                                />
                            </Content>

                            <Footer style={{ textAlign: 'center', background: 'white' }}>
                                Giphy Workshop @ CAMT
                  </Footer>
                        </Layout>
                    </div>
                ) : (
                        <Spin size="large" />
                    )}
                {item ? (
                    <Modal
                        width="40%"
                        style={{ maxHeight: '70%' }}
                        title={item.title}
                        //visible={this.state.isShowModal}
                        visible={this.props.isShowDialog}
                        //onOk={this.onModalClickOK}
                        onCancel={this.onClickCancel}

                        footer={[
                            <Button
                                key="fav"
                                type="primary"
                                icon="heart"
                                size="large"
                                shape="circle"
                                onClick={this.onClickFavorite}
                            />,
                            <Button
                                key="cart"
                                type="primary"
                                icon="shopping-cart"
                                size="large"
                                shape="circle"
                                onClick={this.onClickBuyTicket}
                            />

                        ]}
                    >
                        {item.images != null ? (

                            <img src={item.images.original.url} style={{ height: '100%' }} />
                        ) : (
                                <div></div>
                            )}
                        {/*  */}
                        <br />
                        <br />
                        <p>{item.overview}</p>
                        {/* <img src={item.image_url} style={{ width: '100%' }} />
                        <br />
                        <br />
                        <p>{item.overview}</p> */}
                    </Modal>
                ) : (
                        <div />
                    )}
            </div>

        );
    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Main)