import React, { Component } from 'react';
import { Form, Button, Input, Icon, message, Modal } from 'antd';
import { withRouter } from 'react-router-dom';
import { auth, provider } from '../firebase';
const KEY_USER_DATA = 'user_data';

class Login extends Component {
  state = {
    isLoading: false,
    email: '',
    password: '',
    user: null,
    isShowModal: false,
    isLogin: false,
    imageUrl: ''
  };

  loginFacebook = () => {
    auth.signInWithPopup(provider).then(({ user }) => {
      this.setState({ user })
      localStorage.setItem(
        KEY_USER_DATA,
        JSON.stringify({
          isLoggedIn: true,
          email: user.email
        })
      )
      this.props.history.replace("/movies")
    })
  }

  logoutFacebook = () => {
    auth.signOut().then(() => {
      this.setState({ user: null })
    })
  }

  // ///////////
  navigateToMainPage = () => {
    const { history } = this.props;
    history.push('/movies');
  };

  componentDidMount() {
    const jsonStr = localStorage.getItem(KEY_USER_DATA);
    const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn;
    if (isLoggedIn) {
      this.navigateToMainPage();
    }
  }

  onEmailChange = event => {
    const email = event.target.value;
    this.setState({ email });
  };

  onPasswordChange = event => {
    const password = event.target.value;
    this.setState({ password });
  };

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  saveInformationUser = email => {
    localStorage.setItem(
      KEY_USER_DATA,
      JSON.stringify({
        email: email,
        isLoggedIn: true,
        imageUrl: this.state.imageUrl
      })
    );
    this.setState({ isLoading: false });
    this.navigateToMainPage();
  };

  validatePassword(password) {
    var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
    return re.test(String(password));
  }

  onSubmitFormLogin = e => {
    e.preventDefault();
    this.setState({ isLogin: true });
    const email = this.state.email;
    const password = this.state.password;
    const isEailValid = this.validateEmail(email);
    if (isEailValid) {
      auth
        .signInWithEmailAndPassword(email, password)
        .then(({ user }) => {
          this.setState({ isLogin: false });
          this.saveInformationUser(user.email);
        })
        .catch(_ => {
          this.setState({ isLogin: false });
          this.setState({ isShowModal: true });
        });
    } else {
      this.setState({ isLogin: false });
      message.error('Email or Password invalid!', 1);
    }
  };

  handleCancel = () => {
    this.setState({ isShowModal: false, isLogin: false });
  };

  handleOk = () => {
    this.setState({ isShowModal: false, isLogin: false });
    this.props.history.push('/register');
  };

  gotoRegister = () => {
    this.props.history.push("/register")
  }

  render() {

    return (

      <div style={{ width: '44%', margin: '0 auto' }}>
        <h2>GIPHY</h2>

        <Form onSubmit={this.onSubmitFormLogin}>
          <Form.Item>
            <Input
              prefix={<Icon type="user" />}
              placeholder="Email"
              onChange={this.onEmailChange}
            />
          </Form.Item>

          <Form.Item>
            <Input
              prefix={<Icon type="lock" />}
              type="password"
              placeholder="Password"
              onChange={this.onPasswordChange}
            />
          </Form.Item>

          <Form.Item>
            <Button
              style={{ width: '44%' }}
              htmlType="submit"
              type="primary"ghost
              loading={this.state.isLogin}
            >
              Login
            </Button>
          </Form.Item>
          
        </Form>
        <Form.Item>
            <Button
              style={{ width: '30%' }}
              htmlType="submit"
              type="primary" ghost
              onClick={this.gotoRegister}
            >
              Create new account
            </Button>
          </Form.Item>
          <Form.Item>
            <Button 
            class="ui facebook button" 
            onClick={this.loginFacebook}
            type="primary">
              <Icon type="facebook" />
              Login with Facebook
             </Button>
          </Form.Item>

            <Modal
              title="Something went wrong"
              visible={this.state.isShowModal}
              onOk={this.handleOk}
              onCancel={this.handleCancel}
              footer={[
                <Button key="back" onClick={this.handleCancel}>
                  No
            </Button>,
                <Button key="submit" type="primary" onClick={this.handleOk}>
                  Yes
            </Button>
              ]}
            >
              <p>Sorry! not found this account but you want to create account.</p>
            </Modal>
      </div>
    );
  }
}

export default withRouter(Login);